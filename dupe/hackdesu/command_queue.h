/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __HACKDESU_COMMAND_QUEUE_H__
#define __HACKDESU_COMMAND_QUEUE_H__

#include <stdint.h>

/* a function pointer to a callback that will take a void pointer as parameter */
typedef union tagcq_callback {
	void(*func_ptr)(__inout void *param);
	void *void_ptr;
} cq_callback;

extern const cq_callback cq_no_callback;

/* 
	initializes the command queue.
	must be called before doing anything with the command queue.
	returns zero on failure. 
*/
int cq_init();

/* 
	enqueues the execution of a callback. returns zero on failure.
	the cleanup callback will be called when param needs to be
	deallocated and should perform any cleanup required for param.
	cleanup can be null.
	if the function fails, cleanup will be automatically called. 
*/
int cq_call(__in cq_callback c, __inout_opt void *param, __in_opt cq_callback cleanup);

/* enqueues a memory_write. returns zero on failure. */
int cq_write(__inout uint8_t *pmemory, __in const uint8_t *bytes, int cb);

/* enqueues a memory_write1. returns zero on failure. */
int cq_write1(__inout void *pmemory, uint8_t val);

/* enqueues a memory_write1s. returns zero on failure. */
#define cq_write1s(pmemory, val) cq_write1(pmemory, (uint8_t)val)

/* enqueues a memory_write2. returns zero on failure. */
int cq_write2(__inout void *pmemory, uint16_t val);

/* enqueues a memory_write2s. returns zero on failure. */
#define cq_write2s(pmemory, val) cq_write2(pmemory, (uint16_t)val)

/* enqueues a memory_write4. returns zero on failure. */
int cq_write4(__inout void *pmemory, uint32_t val);

/* enqueues a memory_write4s. returns zero on failure. */
#define cq_write4s(pmemory, val) cq_write2(pmemory, (uint32_t)val)

/* enqueues a memory_write8. returns zero on failure. */
int cq_write8(__inout void *pmemory, uint64_t val);

/* enqueues a memory_write8s. returns zero on failure. */
#define cq_write8s(pmemory, val) cq_write2(pmemory, (uint64_t)val)

/* enqueues a memory_write_jumpE9. returns zero on failure. */
int cq_write_jumpE9(__inout void *pmemory, __in int8_t *dst, int nops);

/* enqueues a memory_deoutr. returns zero on failure. */
int cq_detour(int enabled, __inout void **pptarget, __in void *pdetour);

/* enqueues a memcpy and waits for the command to be processed. returns zero on failure. */
int cq_read(__inout uint8_t *pdst, __in uint8_t *pmemory, int cb);

#define cq_read1(pdst, pmemory) cq_read((uint8_t *)pdst, (uint8_t *)pmemory, sizeof(int8_t))
#define cq_read2(pdst, pmemory) cq_read((uint8_t *)pdst, (uint8_t *)pmemory, sizeof(int16_t))
#define cq_read4(pdst, pmemory) cq_read((uint8_t *)pdst, (uint8_t *)pmemory, sizeof(int32_t))
#define cq_read8(pdst, pmemory) cq_read((uint8_t *)pdst, (uint8_t *)pmemory, sizeof(int64_t))

/* destroys the command queue. must be called on cleanup. */
void cq_destroy();

/* when called, all pending commands will be dequeued and executed. */
void cq_process_all();

#endif
