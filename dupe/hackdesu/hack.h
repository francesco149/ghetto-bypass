/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef HACKDESU_HACK_H
#define HACKDESU_HACK_H

#include <stdint.h>

/* the maximum size allowed by memorypatch for patches */
#define MAX_MEMORYPATCH 16

/* internal struct that contains common members for multiple types of hacks */
typedef struct tagbasic_memory_patch {
	int enabled; 						/* enabled flag */
	int initialized; 					/* true if clean_mem is initialized */
	uint8_t clean_mem[MAX_MEMORYPATCH]; /* will contain the clean memory */
} basic_memory_patch;

/* 
	a simple memory patch with automatic memory back-up.
	addr and patch must be initialized. 
*/
typedef struct tagmemory_patch {
	uint8_t *addr; 						/* address to patch */
	const char *patch;					/* the memory patch as a string e.g "AA BB CC DD". see string_tobytearray for the string format. */
	uint32_t cb; 						/* length of the patch */
	uint8_t edit_mem[MAX_MEMORYPATCH]; 	/* will contain the patch memory */
	basic_memory_patch common; 			/* common properties */
} memory_patch;

/* 
	a simple E9 jump hook.
	addr, hook and nops must be initialized. 
*/
typedef struct taghook {
	uint8_t *addr; 				/* address of the function to hook */
	void *hook; 				/* hook function */
	uint32_t nops; 				/* number of nops after the hook */
	basic_memory_patch common; 	/* common properties */
} hook;

/* 
	a hook using the detours library.
	all members except enabled must be initialized. 
*/
typedef struct tagdetour {
	void **pptarget; 	/* pointer to a function pointer of the function that should be detoured */
	void *hook; 		/* hook function */
	int enabled;
} detour;

/* 
	a hook for a win32 api using the detours library.
	all members except enabled must be initialized. 
*/
typedef struct tagwinapi_detour {
	const char *dll; 		/* dll name */
	const char *func_name; 	/* name of the winapi to hook */
	void **pptarget; 		/* pointer to a function pointer of the function that should be detoured */
	void *hook; 			/* hook function */
	int enabled;
} winapi_detour;

/* possible hack types */
typedef enum taghack_type {
	HACK_MEMPATCH,
	HACK_HOOK,
	HACK_DETOUR,
	HACK_WINAPI_DETOUR,
} hack_type;

/* a generic hack entry that contains a pointer to the hack struct and the type of hack */
typedef struct taggeneric_hack {
	hack_type type;
	void *ptr; /* generic pointer to the specialized hack struct */
} generic_hack;

/* a hack group that contains a list of generic_hack structs that will be toggled on and off all at once */
typedef struct taghack {
	int count; 				/* number of hacks */
	generic_hack *hacks; 	/* array of hacks */
} hack;

/* toggles the hack on and off. returns 0 on failure. */
int hack_toggle(__inout hack *p, int enabled);

/* casts address to T. address is a hex number without the 0x prefix. */
#define addrT(T, address) (T)(0x##address)

/* casts address to void *. address is a hex number without the 0x prefix. */
#define addr(address) addrT(uint8_t *, address)

#endif
