/*
	Copyright 2014 Franc[e]sco (lolisamurai@tfwno.gf)
	This file is part of hackdesu.
	hackdesu is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.
	hackdesu is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.
	You should have received a copy of the GNU General Public License
	along with hackdesu. If not, see <http://www.gnu.org/licenses/>.
*/

#if _DEBUG
#define dbgputs puts
#define dbgprintf printf_s
#define dbgwprintf wprintf_s
#define dbgputchar putchar
#else
#define dbgputs 
#define dbgprintf 
#define dbgwprintf
#define dbgputchar
#endif

#define dbgfnputs dbgprintf(__FUNCDNAME__ ": "); dbgputs
#define dbgfnprintf dbgprintf(__FUNCDNAME__ ": "); dbgprintf
#define dbgfnwprintf dbgprintf(__FUNCDNAME__ ": "); dbgwprintf