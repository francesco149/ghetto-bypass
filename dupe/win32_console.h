#ifndef __WIN32_CONSOLE_H__
#define __WIN32_CONSOLE_H__

#include <windows.h>

/* allocates a console in a Win32 GUI application and redirects stdout to it */
BOOL win32_console_create();

#endif